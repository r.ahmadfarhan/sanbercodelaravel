<?php

trait Hewan{
    public $nama;
    public $darah=50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        return $this->nama." sedang ".$this->keahlian;
    }
}

trait Fight{
    public $attackPower;
    public $defencePower;

    public function serang($to){
        return $this->nama." sedang menyerang ".$to;
    }

    public function diserang($attackPowerPenyerang){
        $this->darah = $this->darah - $attackPowerPenyerang/$this->defencePower;
        return $this->nama." sedang diserang";
    }
}

class Elang{
    use Hewan, Fight;
    
    public function inisiasi(){
        $this->jumlahKaki = 2;
        $this->keahlian = "Terbang Tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function setNama($nama){
        $this->nama=$nama;
        $this->inisiasi();
    }

    public function getNama(){
        return $this->nama;
    }

    public function getAttackPower(){
        return $this->attackPower;
    }

    public function getInfoHewan(){
        return "Nama : ".$this->nama." Darah : ".$this->darah." Jumlah Kaki : ".$this->jumlahKaki.
            " Keahlian : ".$this->keahlian." Attack Power : ".$this->attackPower.
            " Defence Power : ".$this->defencePower.
            " Jenis Hewan : Elang";
    }
}

class Harimau{
    use Hewan, Fight;
    
    public function inisiasi(){
        $this->jumlahKaki = 4;
        $this->keahlian = "Lari Cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function setNama($nama){
        $this->nama=$nama;
        $this->inisiasi();
    }

    public function getNama(){
        return $this->nama;
    }

    public function getAttackPower(){
        return $this->attackPower;
    }

    public function getInfoHewan(){
        return "Nama : ".$this->nama." Darah : ".$this->darah." Jumlah Kaki : ".$this->jumlahKaki.
            " Keahlian : ".$this->keahlian." Attack Power : ".$this->attackPower.
            " Defence Power : ".$this->defencePower.
            " Jenis Hewan : Harimau";
    }
}

$elang = new Elang();
$elang->setNama('Elang_1');

$harimau = new Harimau();
$harimau->setNama('Harimau_1');

echo $elang->getNama();
echo '<br>';
echo $elang->diserang($harimau->getAttackPower());
echo '<br>';
echo $elang->getInfoHewan();
echo '<br>';
echo $elang->atraksi();
echo '<br>';
echo $elang->serang($harimau->getNama());
echo '<br>';

echo $harimau->getNama();
echo '<br>';
echo $harimau->getInfoHewan();
echo '<br>';
echo $harimau->atraksi();
echo '<br>';

?>