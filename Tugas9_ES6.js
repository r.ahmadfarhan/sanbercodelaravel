console.log('No 1 Arrow funtion');
var golden = () => {
    console.log("this is golden!!")
}
golden()


console.log('\n');
console.log('No 2 Object literal');
var newFunction = (firstName, lastName) => {
    return {
        fullName: function(){
            return console.log(firstName + " " + lastName)
        }
    }
}  
//Driver Code 
newFunction("William", "Imoh").fullName()


console.log('\n');
console.log('No 3 Destructuring');
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)


console.log('\n');
console.log('No 4 Array Spreading');
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west, ...east];

//Driver Code
console.log(combined)


console.log('\n');
console.log('No 5 Template Literals');
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code 
console.log(before) 
