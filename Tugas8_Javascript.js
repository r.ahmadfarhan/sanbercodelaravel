// Soal no 1
console.log('No.1');
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

daftarBuah.sort();
for (let i = 0; i < daftarBuah.length; i++) {
    console.log(daftarBuah[i]);
}

// Soal No 2
console.log('No.2');
var kalimat="saya sangat senang belajar javascript";
var array = kalimat.split(' ');
console.log(array);

// Soal no 3
console.log('No.3');
var data_buah = [{
    nama: "strawberry",
    warna: "merah",
    "ada bijinya": "tidak",
    harga: 9000,
}, {
    nama: "jeruk",
    warna: "oranye",
    "ada bijinya": "ada",
    harga: 8000,
}, {
    nama: "Semangka",
    warna: "Hijau & Merah",
    "ada bijinya": "ada",
    harga: 10000,
}, {
    nama: "Pisang",
    warna: "Kuning",
    "ada bijinya": "tidak",
    harga: 5000,
}];

console.log(data_buah[0]);
