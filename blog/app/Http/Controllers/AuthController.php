<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Events\UserRegisteredEvent;
use App\Events\RegenerateOtpEvent;
use App\User;
use App\OtpCode;
use Auth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email|email:rfc,dns',
            'password' => 'required|confirmed|min:8',
        ]);

        $data = [];

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $data['user'] = $user;

        event(new UserRegisteredEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Berhasil register, silahkan cek email',
            'data' => $data,
        ], 200);
    }

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
        // return response()->json(compact('token'));
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function verification(Request $request)
    {
        $request->validate([
            'otp' => 'required',
        ]);

        $otp_code = OtpCode::where('otp', $request->otp)->first();

        if(!$otp_code){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'OTP Code tidak ditemukan',
            ], 200);
        }

        $now = Carbon::now();

        if($now > $otp_code->valid_until){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Kode OTP sudah tidak berlaku, silahkan generate ulang',
            ], 200);
        }

        // update user
        $user = User::find($otp_code->user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();

        // delete otp
        $otp_code->delete();
        $data['user'] = $user;

        return response()->json([
            'response_code' => '01',
            'response_message' => 'Verifikasi Berhasil',
            'data' => $data['user'],
        ], 200);
    }

    public function regenerateOtp(Request $request)
    {
        $request->validate([
            'email' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();
        if(!$user){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email belum terdaftar',
            ], 200);
        }

        $now = Carbon::now();
        $valid_until = $now->addMinutes(60);

        $code = rand(100000, 999999);

        $otp = OtpCode::where('user_id', $user->id)->first();
        if(!$otp){
            $otp = OtpCode::create([
                'otp' => $code,
                'user_id' => $user->id,
                'valid_until' => $valid_until,
            ]);
        }else{
            $otp->otp = $code;
            $otp->valid_until = $valid_until;
            $otp->save();
        }

        $data['otp'] = $otp;

        event(new RegenerateOtpEvent($user));

        return response()->json([
            'response_code' => '01',
            'response_message' => 'Silahkan cek email',
            'data' => $data['otp'],
        ], 200);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required|confirmed|min:8',
        ]);

        $user = User::where('email', $request->email)->first();
        
        if(!$user){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email tidak ditemukan',
            ], 200);
        }

        $user->password = bcrypt($request->password);
        $user->save();

        $data['user'] = $user;
        
        return response()->json([
            'response_code' => '01',
            'response_message' => 'Password Updated',
            'data' => $data['user'],
        ], 200);
    }
}
