Vue.component('comments', {
    template : '#comment-template',
    props : ['comment'],
    data : function(){
        return {
            plus : false,
            minus : false,
        }
    },
    methods : {
        tambah : function(){
            this.plus = !this.plus
            this.minus = false
        },
        kurang : function(){
            this.minus = !this.minus
            this.plus = false
        },
    },
    computed : {
        score : function(){
            if(this.plus)
                return this.comment.score + 1
            else if(this.minus)
                return this.comment.score - 1
            else
                return this.comment.score
        }
    }
})

let vm = new Vue({
    el: '#app',
    data: {
        comments : [
            { message : 'hello', date : '27-08-2020', score : 0, },
            { message : "hello juga", date : '27-08-2020', score : 0, },
        ],
        comment_text : '',
    },

    methods: {
        add : function(){
            this.comments.push({message: this.comment_text, date : '27-08-2020', score : 0,})
            this.comment_text = ''
        },
    }

});