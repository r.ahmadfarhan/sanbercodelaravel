Vue.component('users', {
    template : '#user-template',
    props : ['user'],
    data : function(){
        return {
            plus : false,
            minus : false,
        }
    },
    methods : {
        edit : function(){
            
        },
        hapus : function(){
            // if (index !== -1) {
                // this.user.splice(this.user.indexOf(index), 1);
            // }
        },
    },
    computed : {
        name : function(){
            
        }
    }
})

let vm = new Vue({
    el: '#app',
    data: {
        users : [
            { name : 'Muhammad' },
            { name : "Ahmad" },
            { name : "Mehmed" },
        ],
        user_name : '',
    },

    methods: {
        add : function(){
            this.users.push({name: this.user_name})
            this.user_name = ''
        },
    }
});